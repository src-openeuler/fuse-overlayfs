%define built_tag v1.13
%define built_tag_strip %(b=%{built_tag}; echo ${b:1})
%{!?_modulesloaddir:%global _modulesloaddir %{_usr}/lib/modules-load.d}

Name:             fuse-overlayfs
Version:          1.14
Release:          1
Summary:          FUSE overlay+shiftfs implementation for rootless containers
License:          GPLv3+
URL:              https://github.com/containers/fuse-overlayfs
Source0:          https://github.com/containers/fuse-overlayfs/archive/v1.14.tar.gz
BuildRequires:    autoconf automake fuse3-devel
BuildRequires:    gcc git-core make fuse3
Requires:         kmod fuse3
Provides:         bundled(gnulib) = cb634d40c7b9bbf33fa5198d2e27fdab4c0bf143

%description
%{summary}.

%package devel
Summary: %{summary}
BuildArch: noarch

%description devel
This package contains library source intended for
building other packages which use import path with.

%prep
%autosetup -Sgit  %{name}-%{built_tag_strip}

%build
./autogen.sh
./configure --prefix=%{_prefix} --libdir=%{_libdir}
%{__make}

%install
%make_install
install -d %{buildroot}%{_modulesloaddir}
echo fuse > %{buildroot}%{_modulesloaddir}/fuse-overlayfs.conf

%post
modprobe fuse > /dev/null 2>&1 || :

%check
#define license tag if not already defined
%{!?_licensedir:%global license %doc}

%files
%license COPYING
%doc README.md
%{_bindir}/%{name}
%{_mandir}/man1/*
%{_modulesloaddir}/fuse-overlayfs.conf

%changelog
* Fri Jul 19 2024 zhangxingrong-<zhangxingrong@uniontech.cn> - 1.14-1
- upgrade version to v1.14

* Thu Nov 30 2023 Vicoloa <lvkun@uniontech.com> - 1.13-1
- upgrade version to v1.13

* Wed May 31 2023 duyiwei <duyiwei@kylinos.cn> - 1.12-1
- upgrade version to v1.12

* Fri Feb 03 2023 lilong <lilong@kylinos.cn> - 1.10-1
- Update to v1.10

* Tue Nov 08 2022 duyiwei <duyiwei@kylinos.cn> - 1.9-1
- upgrade version to v1.9

* Tue Jul 26 2022 yuantianyi <yuantianyi@kylinos.cn> - 1.8.1-1
- Update to v1.8.1

* Mon Jan 24 2022 fushanqing <fushanqing@kylinos.cn> - 1.5.0-1
- Initial package
